#include <windows.h>
#include <iostream>
#include <stdio.h>
void StartClone(int nCloneID)
{

    TCHAR szFilename[MAX_PATH] ;
    GetModuleFileName(NULL, szFilename, MAX_PATH) ;
    std::cout<<szFilename;

    TCHAR szCmdLine[MAX_PATH];
    sprintf(szCmdLine,"\"%s\" %d",szFilename,nCloneID);


    STARTUPINFO si;
    ZeroMemory(&si , sizeof(si) ) ;
    si.cb = sizeof(si) ;


    PROCESS_INFORMATION pi;


    BOOL bCreateOK=::CreateProcess(
            szFilename,
            szCmdLine,
            NULL,
            NULL,
            FALSE,
            CREATE_NEW_CONSOLE,
            NULL,
            NULL,
            &si,
            &pi) ;

    // 对子进程释放引用
    if (bCreateOK)
    {
        CloseHandle(pi.hProcess) ;
        CloseHandle(pi.hThread) ;
    }
}

int main(int argc, char* argv[] )
{
    int nClone=0;
    if (argc > 1)
    {
        :: sscanf(argv[1] , "%d" , &nClone) ;
    }
    std :: cout << "Process ID:" << :: GetCurrentProcessId()
                << ", Clone ID:" << nClone
                << std :: endl;
    const int c_nCloneMax=3;
    if (nClone < c_nCloneMax)
    {
        StartClone(++nClone) ;
    }
    getchar();
    return 0;
}
